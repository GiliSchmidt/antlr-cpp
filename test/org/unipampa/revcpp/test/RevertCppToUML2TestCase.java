package org.unipampa.revcpp.test;

import org.adapit.wctoolkit.infrastructure.events.actions.xmi.ExportXMIAsAction;
import org.adapit.wctoolkit.junit.FomdaTestCase;
import org.adapit.wctoolkit.uml.ext.fomda.metamodel.annotations.FomdaTask;
import org.adapit.wctoolkit.uml.ext.fomda.metamodel.annotations.IOKind;
import org.adapit.wctoolkit.uml.ext.fomda.metamodel.annotations.IOTask;
import org.adapit.wctoolkit.uml.ext.fomda.metamodel.annotations.WctFileHandlerKind;
import org.junit.Test;
import org.unipampa.revcpp.parser.BaseParser;
import org.unipampa.revcpp.parser.CppVisitorParser;


@FomdaTask(
	tasks= {
	 @IOTask(inputFilePath="config/start1.fomda",fileKind=WctFileHandlerKind.FOMDA,kind=IOKind.INPUT)
	/** @IOTask(outputFilePath="output/modelorevertido.xmi",
	 fileKind=WctFileHandlerKind.XMI,
	 kind=IOKind.OUTPUT,
	 outputOperationName="getModel")**/
	}		
)
public class RevertCppToUML2TestCase extends FomdaTestCase{

	private org.wct.uml.Model model;
	
	@Test
	public void testaParser() {
		String filePath = ".\\Test files\\generalization.cpp";
		
		  System.setProperty("java.util.logging.SimpleFormatter.format", 
		            "\n%4$s %5$s%6$s%n");

		try {
			BaseParser parser = new CppVisitorParser(filePath);
			
			model =parser.parse();
			
			ExportXMIAsAction action = new ExportXMIAsAction();
			action.saveAsXMI1_2(model, "output/modelorevertido.xmi");
			
			assertNotNull(model);

		} catch (Exception ex) {

			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
	}

	public org.wct.uml.Model getModel() {
		return model;
	}

	public void setModel(org.wct.uml.Model model) {
		this.model = model;
	}
	
}
