package org.unipampa.revcpp.test;

import java.io.IOException;

import org.unipampa.revcpp.parser.CppVisitorParser;

public class ShowGuiAstTest {

	public static void main(String[] args) throws IOException {
		String filePath = "./Test files/class.cpp";
		CppVisitorParser parser = new CppVisitorParser(filePath);
		
		parser.showGraphicTree();
	}
}
