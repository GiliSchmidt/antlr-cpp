package org.unipampa.revcpp.test;

import org.adapit.wctoolkit.tool.design.junit.WctUmlModuleTestCase;
import org.adapit.wctoolkit.uml.ext.fomda.metamodel.annotations.FomdaTask;
import org.adapit.wctoolkit.uml.ext.fomda.metamodel.annotations.IOKind;
import org.adapit.wctoolkit.uml.ext.fomda.metamodel.annotations.IOTask;
import org.adapit.wctoolkit.uml.ext.fomda.metamodel.annotations.WctFileHandlerKind;
import org.junit.Test;

@FomdaTask(tasks = {
		@IOTask(inputFilePath = "config/start1.fomda", kind = IOKind.INPUT, fileKind = WctFileHandlerKind.FOMDA),
		@IOTask(inputFilePath = "output/modelorevertido.xmi", kind = IOKind.INPUT, fileKind = WctFileHandlerKind.XMI) })
public class ShowRevertCppToUML2TestCase extends WctUmlModuleTestCase {
	@Test()
	public void testInputModelExists() throws Exception {
		assertNotNull(super.getInputElement(0));
	}
}
