package org.unipampa.revcpp.parser;


import java.io.IOException;

import org.unipampa.revcpp.parser.visitor.ClassSpecifierVisitor;
import org.wct.uml.Model;
import org.wct.uml.impl.ElementFactory;

public class CppVisitorParser extends BaseParser {

	public CppVisitorParser(String filePath) throws IOException {
		super(filePath);
	}

	@Override
	public Model parse() {
		Model rootModel = ElementFactory.instance().createModel();
		rootModel.setName("CPP Model");

		ClassSpecifierVisitor classSpecifierVisitor = new ClassSpecifierVisitor(rootModel);
		classSpecifierVisitor.visit(parser.translationunit());
		return rootModel;
	}



	

	

	


	



}
