package org.unipampa.revcpp.parser.visitor;

import org.wct.uml.Model;
import org.wct.uml.enums.VisibilityKind;
import org.wct.uml.Class;

abstract class BaseClassMemberVisitor<T> extends BaseClassVisitor<T> {

	protected VisibilityKind visibility;

	public BaseClassMemberVisitor(Model model, Class clas, VisibilityKind visibility) {
		super(model, clas);
		this.visibility = visibility;
	}

	

}