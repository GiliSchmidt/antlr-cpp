package org.unipampa.revcpp.parser.visitor;

import org.unipampa.revcpp.antlr.CPP14Parser.FunctiondefinitionContext;
import org.unipampa.revcpp.antlr.CPP14Parser.ParameterdeclarationContext;
import org.wct.uml.Class;
import org.wct.uml.Model;
import org.wct.uml.Operation;
import org.wct.uml.Parameter;
import org.wct.uml.Type;
import org.wct.uml.enums.VisibilityKind;
import org.wct.uml.impl.ElementFactory;

/**
 * Para recuperar informações de funções
 * 
 * @author Giliardi Schmidt, Guilherme Bolfe
 *
 */
class FunctionDefinitionVisitor extends BaseClassMemberVisitor<Object> {

	public FunctionDefinitionVisitor(Model model, Class clas, VisibilityKind visibility) {
		super(model, clas, visibility);
	}

	@Override
	public Object visitFunctiondefinition(FunctiondefinitionContext ctx) {
		Type returnType = getUmlType(getLastChildText(ctx.declspecifierseq()));
		String functionName = getLastChildText(ctx.declarator());

		try {
			logger.info("Creating Operation \n father is: " + clas.getName() + ", \n name: " + functionName
					+ " \n return type: " + returnType.getAsText() + "\n visibility: " + visibility.getLiteral());

			Operation op = ElementFactory.instance().createOperation(clas, functionName);
			clas.addOperation(op);

			op.setVisibility(visibility);

			Parameter ret = ElementFactory.instance().createParameterReturn(op);
			ret.setType(returnType);
			op.addParameter(ret);

			ParameterDeclarationVisitor declarationVisitor = new ParameterDeclarationVisitor(model, clas, visibility,
					op);
			ctx.accept(declarationVisitor);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return super.visitFunctiondefinition(ctx);
	}

	/**
	 * Para recuperar informações de parametros de uma função
	 * 
	 * @author Giliardi Schmidt, Guilherme Bolfe
	 *
	 */
	private class ParameterDeclarationVisitor extends BaseClassMemberVisitor<Object> {

		private Operation op;

		public ParameterDeclarationVisitor(Model model, Class clas, VisibilityKind visibility, Operation op) {
			super(model, clas, visibility);
			this.op = op;
		}

		@Override
		public Object visitParameterdeclaration(ParameterdeclarationContext ctx) {
			String parameterName = getLastChildText(ctx.declarator());
			Type parameterType = getUmlType(getLastChildText(ctx.declspecifierseq()));

			try {
				logger.info("Creating Operation parameter \n father is: " + op.getName() + ", \n name: " + parameterName
						+ " \n type: " + parameterType);

				Parameter parameter = ElementFactory.instance().createParameter(op);

				parameter.setName(parameterName);
				parameter.setType(parameterType);

				op.addParameter(parameter);

			} catch (Exception e) {
				e.printStackTrace();
			}

			return super.visitParameterdeclaration(ctx);
		}

	}

}