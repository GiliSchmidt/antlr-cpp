package org.unipampa.revcpp.parser.visitor;

import org.antlr.v4.runtime.tree.ParseTree;
import org.unipampa.revcpp.antlr.CPP14Parser.FunctiondefinitionContext;
import org.unipampa.revcpp.antlr.CPP14Parser.MemberdeclarationContext;
import org.wct.uml.Attribute;
import org.wct.uml.Class;
import org.wct.uml.Model;
import org.wct.uml.Type;
import org.wct.uml.enums.VisibilityKind;
import org.wct.uml.impl.ElementFactory;

/**
 * Para navegar pelos membros de uma classe
 * 
 * @author gilis
 *
 */
class MemberDeclarationVisitor extends BaseClassMemberVisitor<Object> {

	public MemberDeclarationVisitor(Model model, Class clas, VisibilityKind visibility) {
		super(model, clas, visibility);
	}

	@Override
	public Object visitMemberdeclaration(MemberdeclarationContext ctx) {
		for (ParseTree tree : ctx.children) {
			//se for uma função, aplica o visitor para funções
			//caso contrario, é um atributo
			if (tree instanceof FunctiondefinitionContext) {
				FunctionDefinitionVisitor definitionVisitor = new FunctionDefinitionVisitor(model, clas, visibility);
				ctx.accept(definitionVisitor);

				return super.visitMemberdeclaration(ctx);
			}
		}

		Type attributeType = getUmlType(getLastChildText(ctx.declspecifierseq()));
		String attributeName = getLastChildText(ctx.memberdeclaratorlist());

		try {
			logger.info("Creating Attribute with parameters: \n" + visibility.name() + " " + attributeName + "::"
					+ attributeType.getAsText() + " \n Father is: " + clas.getName());

			if (attributeType instanceof org.wct.uml.Class) {
				ElementFactory.instance().createAssociation(model, clas, (Class) attributeType,
						new String[] { visibility.getLiteral() + " " + clas.getName(), "1" },
						new String[] { visibility.getLiteral() + " " + attributeType.getName(), "1" });
			} else {
				Attribute attribute = ElementFactory.instance().createAttribute(clas);

				attribute.setName(attributeName);
				attribute.setType(attributeType);
				attribute.setVisibility(visibility);
				
				clas.addAttribute(attribute);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return super.visitMemberdeclaration(ctx);
	}

}