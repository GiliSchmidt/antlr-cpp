package org.unipampa.revcpp.parser.visitor;

import org.unipampa.revcpp.antlr.CPP14Parser.ClassnameContext;
import org.unipampa.revcpp.antlr.CPP14Parser.ClassspecifierContext;
import org.wct.uml.Class;
import org.wct.uml.Model;
import org.wct.uml.impl.ElementFactory;

/**
 * Entry point para recuperar informações sobre as classes
 * @author Giliardi Schmidt, Guilherme Bolfe
 *
 */
public class ClassSpecifierVisitor extends BaseVisitor<Class> {

	public ClassSpecifierVisitor(Model model) {
		super(model);
	}

	@Override
	public Class visitClassspecifier(ClassspecifierContext ctx) {
		Class c = null;

		//nome da classe
		String name = ctx.classhead().classheadname().getText();

		try {
			c = super.createClass(name);

			//se ctx.classhead().baseclause() != null então a classe estende outra classe
			if (ctx.classhead().baseclause() != null) {
				ClassNameVisitor classNameVisitor = new ClassNameVisitor(model, c);
				ctx.classhead().baseclause().accept(classNameVisitor);
			}

			MemberSpecificationVisitor memberSpecificationVisitor = new MemberSpecificationVisitor(model, c);
			ctx.accept(memberSpecificationVisitor);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return c;
	}

	/**
	 * Para recuperar informações de generalização
	 * @author Giliardi Schmidt, Guilherme Bolfe
	 *
	 */
	private class ClassNameVisitor extends BaseVisitor<Object> {
		private Class clas;

		public ClassNameVisitor(Model model, Class clas) {
			super(model);
			this.clas = clas;
		}

		@Override
		public Object visitClassname(ClassnameContext ctx) {
			String className = ctx.getText();
			Class c = (Class) super.getUmlType(className);
			try {
				ElementFactory.instance().createGeneralization(model, clas, c);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

	}

}