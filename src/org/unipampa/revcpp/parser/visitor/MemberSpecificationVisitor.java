package org.unipampa.revcpp.parser.visitor;

import org.unipampa.revcpp.antlr.CPP14Parser.MemberspecificationContext;
import org.wct.uml.Class;
import org.wct.uml.Model;
import org.wct.uml.enums.VisibilityKind;

/**
 * Para navegar pelo conteudo da classe (atributos, metodos, etc)
 * 
 * @author gilis
 *
 */
class MemberSpecificationVisitor extends BaseClassVisitor<Object> {

	public MemberSpecificationVisitor(Model model, Class c) {
		super(model, c);

	}

	@Override
	public Object visitMemberspecification(MemberspecificationContext ctx) {
		if (ctx.accessspecifier() == null) {
			return super.visitMemberspecification(ctx);
		}

		VisibilityKind visibility = null;

		String visibilityString = ctx.accessspecifier().getText();

		switch (visibilityString) {
		case "public":
			visibility = VisibilityKind.PUBLIC_LITERAL;
			break;
		case "private":
			visibility = VisibilityKind.PRIVATE_LITERAL;
			break;
		}

		MemberDeclarationVisitor declarationVisitor = new MemberDeclarationVisitor(model, clas, visibility);
		ctx.accept(declarationVisitor);

		return super.visitMemberspecification(ctx);
	}

}