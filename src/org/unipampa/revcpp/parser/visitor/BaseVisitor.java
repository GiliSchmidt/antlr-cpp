package org.unipampa.revcpp.parser.visitor;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.antlr.v4.runtime.tree.ParseTree;
import org.unipampa.revcpp.antlr.CPP14BaseVisitor;
import org.wct.uml.Class;
import org.wct.uml.Model;
import org.wct.uml.Type;
import org.wct.uml.impl.ElementFactory;

abstract class BaseVisitor<T> extends CPP14BaseVisitor<T> {

	protected static Map<String, Type> TYPESINMODEL = new HashMap<String, Type>();

	protected Model model;
	protected final Logger logger;

	public BaseVisitor(Model model) {
		this.model = model;
		this.logger = Logger.getLogger("INFO LOG");
	}

	protected String getLastChildText(ParseTree parseTree) {
		ParseTree aux = parseTree;

		while (aux.getChildCount() > 0) {
			aux = aux.getChild(0);
		}

		return aux.getText();
	}

	protected Class createClass(String name) {
		Type aux = TYPESINMODEL.get(name);

		if (aux == null) {
			logger.info("Creating Class\n Name: " + name);
			try {
				aux = ElementFactory.instance().createClass(model, name);

				TYPESINMODEL.putIfAbsent(name, aux);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return (Class) aux;
	}

	protected Type getUmlType(String type) {
		Type aux = TYPESINMODEL.get(type);

		if (aux != null) {
			return aux;
		}

		logger.info("Creating Type\n Name: " + type);
		try {
			switch (type) {
			case "int":
				aux = ElementFactory.instance().createPrimitiveType(model, "int");
				break;
			case "double":
				aux = ElementFactory.instance().createPrimitiveType(model, "double");
				break;
			case "string":
				aux = ElementFactory.instance().createPrimitiveType(model, "string");
				break;
			case "void":
				aux = ElementFactory.instance().createPrimitiveType(model, "void");
				break;
			default:
				// case it's not a primitive type
				aux = createClass(type);
				break;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		TYPESINMODEL.putIfAbsent(type, aux);

		return aux;
	}
}