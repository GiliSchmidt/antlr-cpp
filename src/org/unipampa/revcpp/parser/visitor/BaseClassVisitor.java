package org.unipampa.revcpp.parser.visitor;

import org.wct.uml.Model;
import org.wct.uml.Class;

abstract class BaseClassVisitor<T> extends BaseVisitor<T> {

		protected Class clas;

		public BaseClassVisitor(Model model, Class clas) {
			super(model);

			this.clas = clas;
		}
		

	}