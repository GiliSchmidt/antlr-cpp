package org.unipampa.revcpp.parser;

import java.io.IOException;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.unipampa.revcpp.antlr.CPP14Lexer;
import org.unipampa.revcpp.antlr.CPP14Parser;
import org.wct.uml.Model;

public abstract class BaseParser {

	protected CPP14Parser parser;

	public BaseParser(String filePath) throws IOException {
		CharStream charStream = CharStreams.fromFileName(filePath);
		CPP14Lexer lexer = new CPP14Lexer(charStream);
		CommonTokenStream commonTokenStream = new CommonTokenStream(lexer);
		this.parser = new CPP14Parser(commonTokenStream);
	}

	/**
	 * Exibe a GUI da AST em um JFrame.
	 * 
	 * @author Giliardi Schmidt
	 *
	 */
	public void showGraphicTree() {
		JFrame frame = new JFrame("Antlr AST");
		JPanel panel = new JPanel();

		TreeViewer viewr = new TreeViewer(Arrays.asList(parser.getRuleNames()), parser.translationunit());

		viewr.setScale(1.5);

		viewr.setScale(1.5);
		panel.add(viewr);
		frame.add(panel);
		frame.setSize(800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JScrollPane pane = new JScrollPane(panel);
		frame.getContentPane().add(pane);

		frame.setVisible(true);

	}

	/**
	 * Realizar o parse do arquivo cpp informado na construção do objeto para um UML
	 * Model
	 * 
	 * @param path
	 *            Caminho para o arquivo
	 * @return
	 */
	public abstract Model parse();

}
