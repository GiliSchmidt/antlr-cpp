class Person {
   public:
      string name;

      void speak(string phrase){
          //do something
      }
};

class Professor: Person{
    public:
        double salary;

        void teach(){
            //do something
        }
};