class Person {
   public:
      string name;   
      int height;

    int doSomething(int a){
        return 1;
    }
};

class Consumer{
    private:
        void consume(){

        }
};


class Professor: Person, Consumer{
    public:
        int phoneNumber;
        Person person;
};
